object DMAutor: TDMAutor
  OldCreateOrder = False
  Height = 150
  Width = 285
  object FDQAutor: TFDQuery
    Connection = DMConexao.FDConexao
    SQL.Strings = (
      'select * from cad_autor')
    Left = 80
    Top = 48
    object FDQAutorid_autor: TIntegerField
      FieldName = 'id_autor'
      Origin = 'id_autor'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQAutornome: TWideStringField
      FieldName = 'nome'
      Origin = 'nome'
      Size = 45
    end
  end
  object DSAutor: TDataSource
    DataSet = FDQAutor
    OnStateChange = DSAutorStateChange
    Left = 136
    Top = 32
  end
  object FDQComboAutor: TFDQuery
    Connection = DMConexao.FDConexao
    SQL.Strings = (
      'select * from cad_autor')
    Left = 215
    Top = 48
  end
end
