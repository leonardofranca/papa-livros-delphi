unit UDMEditora;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TDMEditora = class(TDataModule)
    FDQEditora: TFDQuery;
    DSEditora: TDataSource;
    FDQComboEditora: TFDQuery;
    FDQEditoraid_editora: TIntegerField;
    FDQEditoranome: TWideStringField;
    FDQEditoracnpj: TWideStringField;
    procedure DSEditoraStateChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMEditora: TDMEditora;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UCadEditora;

{$R *.dfm}

procedure TDMEditora.DSEditoraStateChange(Sender: TObject);
begin
with FCadEditora do
   begin
       BNovo.Enabled := DSEditora.State in [dsBrowse];
      BSalvar.Enabled := DSEditora.State in [dsInsert, dsEdit];
      BCancelar.Enabled := Bsalvar.Enabled;
      BEditar.Enabled := (BNovo.Enabled) and (not(FDQEditora.IsEmpty));
      BExcluir.Enabled := BEditar.Enabled;
    end;
end;

end.
