object DMUsuarios: TDMUsuarios
  OldCreateOrder = False
  Height = 150
  Width = 215
  object FDQUsuarios: TFDQuery
    Connection = DMConexao.FDConexao
    SQL.Strings = (
      'select * from cad_usuarios')
    Left = 72
    Top = 56
    object FDQUsuariosid_usuario: TIntegerField
      FieldName = 'id_usuario'
      Origin = 'id_usuario'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQUsuariosnm_usuario: TWideStringField
      FieldName = 'nm_usuario'
      Origin = 'nm_usuario'
      Size = 40
    end
    object FDQUsuarioslogin: TWideStringField
      FieldName = 'login'
      Origin = '"login"'
    end
    object FDQUsuariossenha: TWideStringField
      FieldName = 'senha'
      Origin = 'senha'
    end
    object FDQUsuariosnivel: TWideStringField
      FieldName = 'nivel'
      Origin = 'nivel'
      Size = 50
    end
  end
end
