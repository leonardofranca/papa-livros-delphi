object FCadLivros: TFCadLivros
  Left = 0
  Top = 0
  Caption = 'Cadastro de Livros'
  ClientHeight = 294
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label8: TLabel
    Left = 16
    Top = 152
    Width = 31
    Height = 13
    Caption = 'Label8'
  end
  object Label10: TLabel
    Left = 8
    Top = 196
    Width = 27
    Height = 13
    Caption = 'Autor'
  end
  object PCLivros: TPageControl
    Left = -2
    Top = -1
    Width = 449
    Height = 298
    ActivePage = TCadastro
    TabOrder = 0
    object TCadastro: TTabSheet
      Caption = 'Cadastro'
      object Label1: TLabel
        Left = 7
        Top = 3
        Width = 34
        Height = 13
        Caption = 'id_livro'
        FocusControl = DBEdit1
      end
      object Label2: TLabel
        Left = 95
        Top = 3
        Width = 24
        Height = 13
        Caption = 'titulo'
        FocusControl = DBEdit2
      end
      object Label3: TLabel
        Left = 7
        Top = 46
        Width = 45
        Height = 13
        Caption = 'descricao'
        FocusControl = DBEdit3
      end
      object Label4: TLabel
        Left = 358
        Top = 89
        Width = 30
        Height = 13
        Caption = 'status'
        FocusControl = DBEdit4
      end
      object Label5: TLabel
        Left = 7
        Top = 89
        Width = 54
        Height = 13
        Caption = 'quantidade'
        FocusControl = DBEdit5
      end
      object Label6: TLabel
        Left = 232
        Top = 89
        Width = 78
        Height = 13
        Caption = 'data_publicacao'
        FocusControl = DBEdit6
      end
      object Autor: TLabel
        Left = 7
        Top = 132
        Width = 27
        Height = 13
        Caption = 'Autor'
      end
      object Label9: TLabel
        Left = 232
        Top = 132
        Width = 34
        Height = 13
        Caption = 'Editora'
      end
      object Label11: TLabel
        Left = 7
        Top = 178
        Width = 47
        Height = 13
        Caption = 'Categoria'
      end
      object BNovo: TBitBtn
        Left = 7
        Top = 233
        Width = 75
        Height = 25
        Caption = 'Novo'
        TabOrder = 0
        OnClick = BNovoClick
      end
      object BEditar: TBitBtn
        Left = 92
        Top = 233
        Width = 75
        Height = 25
        Caption = 'Editar'
        TabOrder = 1
        OnClick = BEditarClick
      end
      object BSalvar: TBitBtn
        Left = 182
        Top = 233
        Width = 75
        Height = 25
        Caption = 'Salvar'
        TabOrder = 2
        OnClick = BSalvarClick
      end
      object BCancelar: TBitBtn
        Left = 271
        Top = 233
        Width = 75
        Height = 25
        Caption = 'Cancelar'
        TabOrder = 3
        OnClick = BCancelarClick
      end
      object BExcluir: TBitBtn
        Left = 358
        Top = 233
        Width = 75
        Height = 25
        Caption = 'Excluir'
        TabOrder = 4
        OnClick = BExcluirClick
      end
      object DBEdit1: TDBEdit
        Left = 7
        Top = 19
        Width = 82
        Height = 21
        DataField = 'id_livro'
        DataSource = DSLivros
        Enabled = False
        TabOrder = 5
      end
      object DBEdit2: TDBEdit
        Left = 95
        Top = 19
        Width = 343
        Height = 21
        DataField = 'titulo'
        DataSource = DSLivros
        TabOrder = 6
      end
      object DBEdit3: TDBEdit
        Left = 7
        Top = 62
        Width = 431
        Height = 21
        DataField = 'descricao'
        DataSource = DSLivros
        TabOrder = 7
      end
      object DBEdit4: TDBEdit
        Left = 358
        Top = 105
        Width = 80
        Height = 21
        DataField = 'status'
        DataSource = DSLivros
        TabOrder = 8
      end
      object DBEdit5: TDBEdit
        Left = 7
        Top = 105
        Width = 219
        Height = 21
        DataField = 'quantidade'
        DataSource = DSLivros
        TabOrder = 9
      end
      object DBEdit6: TDBEdit
        Left = 232
        Top = 105
        Width = 120
        Height = 21
        DataField = 'data_publicacao'
        DataSource = DSLivros
        TabOrder = 10
      end
      object DBLookupComboBox1: TDBLookupComboBox
        Left = 7
        Top = 151
        Width = 219
        Height = 21
        DataField = 'autor_id'
        DataSource = DSLivros
        KeyField = 'id_autor'
        ListField = 'nome'
        ListSource = DSAutor
        TabOrder = 11
      end
      object DBLookupComboBox2: TDBLookupComboBox
        Left = 232
        Top = 151
        Width = 206
        Height = 21
        DataField = 'editora_id'
        DataSource = DSLivros
        KeyField = 'id_editora'
        ListField = 'nome'
        ListSource = DSEditora
        TabOrder = 12
      end
      object DBLookupComboBox3: TDBLookupComboBox
        Left = 7
        Top = 197
        Width = 431
        Height = 21
        DataField = 'categoria_id'
        DataSource = DSLivros
        KeyField = 'id_editora'
        ListField = 'nome'
        ListSource = DSCategoria
        TabOrder = 13
      end
    end
    object TConsulta: TTabSheet
      Caption = 'Consulta'
      ImageIndex = 1
      object Label7: TLabel
        Left = 40
        Top = 128
        Width = 31
        Height = 13
        Caption = 'Label7'
      end
      object EPesquisar: TEdit
        Left = 19
        Top = 16
        Width = 294
        Height = 21
        TabOrder = 0
      end
      object BBuscar: TBitBtn
        Left = 328
        Top = 12
        Width = 99
        Height = 25
        Caption = 'Buscar'
        TabOrder = 1
        OnClick = BBuscarClick
      end
      object DBGrid1: TDBGrid
        Left = 19
        Top = 43
        Width = 408
        Height = 167
        DataSource = DSLivros
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = DBGrid1DblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'id_livro'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'titulo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'descricao'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'status'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'quantidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'data_publicacao'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'autor_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'categoria_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'editora_id'
            Visible = True
          end>
      end
    end
  end
  object DSLivros: TDataSource
    DataSet = DMLivros.FDQLivros
    OnStateChange = DSLivrosStateChange
    Left = 88
    Top = 240
  end
  object DSAutor: TDataSource
    DataSet = DMAutor.FDQComboAutor
    Left = 168
    Top = 240
  end
  object DSEditora: TDataSource
    DataSet = DMEditora.FDQComboEditora
    Left = 248
    Top = 240
  end
  object DSCategoria: TDataSource
    DataSet = DMEditora.FDQComboEditora
    Left = 338
    Top = 239
  end
end
