unit UDMEmprestimo;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, frxClass, frxDBSet,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, frxExportBaseDialog,
  frxExportPDF;

type
  TDMEmprestimo = class(TDataModule)
    rEmprestimos: TfrxReport;
    frxPDFExport1: TfrxPDFExport;
    FDQAutor: TFDQuery;
    FDQLivros: TFDQuery;
    FDQLivrosid_livro: TIntegerField;
    FDQLivrostitulo: TWideStringField;
    FDQLivrosdescricao: TWideStringField;
    FDQLivrosstatus: TWideStringField;
    FDQLivrosquantidade: TIntegerField;
    FDQLivrosdata_publicacao: TDateField;
    FDQLivrosautor_id: TIntegerField;
    FDQLivroscategoria_id: TIntegerField;
    FDQLivroseditora_id: TIntegerField;
    frxDsAutor: TfrxDBDataset;
    frxDSLivros: TfrxDBDataset;
    FDQEmprestimo: TFDQuery;
    FDQEmprestimoLivro: TFDQuery;
    FDQUsuario: TFDQuery;
    FDSA: TFDSchemaAdapter;
    FDQBuscaLivros: TFDQuery;
    FDQUsuarioid_usuario: TIntegerField;
    FDQUsuarionm_usuario: TWideStringField;
    FDQUsuariologin: TWideStringField;
    FDQUsuariosenha: TWideStringField;
    FDQUsuarionivel: TWideStringField;
    FDQUsuariosexo: TWideStringField;
    FDQUsuariodt_nascimento: TDateField;
    FDQBuscaLivrosid_livro: TIntegerField;
    FDQBuscaLivrostitulo: TWideStringField;
    FDQBuscaLivrosdescricao: TWideStringField;
    FDQBuscaLivrosstatus: TWideStringField;
    FDQBuscaLivrosquantidade: TIntegerField;
    FDQBuscaLivrosdata_publicacao: TDateField;
    FDQBuscaLivrosautor_id: TIntegerField;
    FDQBuscaLivroscategoria_id: TIntegerField;
    FDQBuscaLivroseditora_id: TIntegerField;
    FDQEmprestimoLivroid_livro: TIntegerField;
    FDQEmprestimoLivroid_emprestimo: TIntegerField;
    FDQEmprestimoLivroquantidade: TIntegerField;
    FDQEmprestimoLivrodt_devolucao: TDateField;
    FDQEmprestimoid_emprestimo: TIntegerField;
    FDQEmprestimoid_usuario: TIntegerField;
    FDQEmprestimodt_emprestimo: TDateField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMEmprestimo: TDMEmprestimo;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UDMConexao;

{$R *.dfm}

end.
