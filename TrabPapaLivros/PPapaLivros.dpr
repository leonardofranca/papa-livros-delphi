program PPapaLivros;

uses
  Vcl.Forms,
  UPrincipal in 'UPrincipal.pas' {FPrincipal},
  UDMConexao in 'UDMConexao.pas' {DMConexao: TDataModule},
  ULogin in 'ULogin.pas' {FLogin},
  UCadUsuarios in 'UCadUsuarios.pas' {FCadUsuarios},
  UDMUsuarios in 'UDMUsuarios.pas' {DMUsuarios: TDataModule},
  UDMConAutor in 'UDMConAutor.pas' {DMAutor: TDataModule},
  UCadAutor in 'UCadAutor.pas' {FAutores},
  UDMCategoria in 'UDMCategoria.pas' {DMCategoria: TDataModule},
  UCadCategoria in 'UCadCategoria.pas' {FCadCategoria},
  UDMEditora in 'UDMEditora.pas' {DMEditora: TDataModule},
  UCadEditora in 'UCadEditora.pas' {FCadEditora},
  UDMLivros in 'UDMLivros.pas' {DMLivros: TDataModule},
  UCadLivros in 'UCadLivros.pas' {FCadLivros},
  UDMEmprestimo in 'UDMEmprestimo.pas' {DMEmprestimo: TDataModule},
  UCadEmprestimo in 'UCadEmprestimo.pas' {FCadEmprestimo};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDMConexao, DMConexao);
  Application.CreateForm(TFPrincipal, FPrincipal);
  Application.CreateForm(TDMAutor, DMAutor);
  Application.CreateForm(TFAutores, FAutores);
  Application.CreateForm(TDMCategoria, DMCategoria);
  Application.CreateForm(TFCadCategoria, FCadCategoria);
  Application.CreateForm(TDMEditora, DMEditora);
  Application.CreateForm(TFCadEditora, FCadEditora);
  Application.CreateForm(TDMLivros, DMLivros);
  Application.CreateForm(TFCadLivros, FCadLivros);
  Application.CreateForm(TDMEmprestimo, DMEmprestimo);
  Application.CreateForm(TFCadEmprestimo, FCadEmprestimo);
  Application.Run;
end.
