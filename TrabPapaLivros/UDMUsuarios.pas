unit UDMUsuarios;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TDMUsuarios = class(TDataModule)
    FDQUsuarios: TFDQuery;
    FDQUsuariosid_usuario: TIntegerField;
    FDQUsuariosnm_usuario: TWideStringField;
    FDQUsuarioslogin: TWideStringField;
    FDQUsuariossenha: TWideStringField;
    FDQUsuariosnivel: TWideStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMUsuarios: TDMUsuarios;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
