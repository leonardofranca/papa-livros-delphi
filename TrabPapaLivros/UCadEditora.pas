unit UCadEditora;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls, Vcl.Buttons, Vcl.ComCtrls;

type
  TFCadEditora = class(TForm)
    PCEditora: TPageControl;
    TCadastro: TTabSheet;
    BNovo: TBitBtn;
    BEditar: TBitBtn;
    BSalvar: TBitBtn;
    BCancelar: TBitBtn;
    BExcluir: TBitBtn;
    TConsulta: TTabSheet;
    EPesquisar: TEdit;
    BPesquisar: TBitBtn;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    procedure BNovoClick(Sender: TObject);
    procedure BEditarClick(Sender: TObject);
    procedure BSalvarClick(Sender: TObject);
    procedure BExcluirClick(Sender: TObject);
    procedure BCancelarClick(Sender: TObject);
    procedure BPesquisarClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadEditora: TFCadEditora;

implementation

{$R *.dfm}

uses UCadCategoria, UDMCategoria, UDMEditora;

procedure TFCadEditora.BCancelarClick(Sender: TObject);
begin
  DMEditora.FDQEditora.Cancel;
end;

procedure TFCadEditora.BEditarClick(Sender: TObject);
begin
  DMEditora.FDQEditora.Edit;
end;

procedure TFCadEditora.BExcluirClick(Sender: TObject);
begin
  DMEditora.FDQEditora.Delete;
end;

procedure TFCadEditora.BNovoClick(Sender: TObject);
begin
  DMEditora.FDQEditora.Insert;
end;

procedure TFCadEditora.BPesquisarClick(Sender: TObject);
begin
  with DMEditora do
  begin
    FDQEditora.Close;
    FDQEditora.SQL.Clear;
    FDQEditora.SQL.Add('select * from cad_editora where nome like :Peditora ');
    FDQEditora.ParamByName('Peditora').Value := '%' + EPesquisar.Text + '%';
    FDQEditora.Open;
  end;
end;

procedure TFCadEditora.BSalvarClick(Sender: TObject);
begin
  DMEditora.FDQEditora.Post;
end;

procedure TFCadEditora.DBGrid1DblClick(Sender: TObject);
begin
  PCEditora.ActivePage := TCadastro;
end;

procedure TFCadEditora.FormActivate(Sender: TObject);
begin
with DMEditora do
  begin
    FDQEditora.Close;
    FDQEditora.SQL.Clear;
    FDQEditora.SQL.Add('select * from cad_editora');
    FDQEditora.Open;
  end;

  PCEditora.ActivePage := TCadastro;
end;

procedure TFCadEditora.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FCadEditora := nil;
  FCadEditora.Free;
  DMEditora := nil;
  DMEditora.Free;
end;

end.
