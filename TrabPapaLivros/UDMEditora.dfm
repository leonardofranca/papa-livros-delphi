object DMEditora: TDMEditora
  OldCreateOrder = False
  Height = 154
  Width = 339
  object FDQEditora: TFDQuery
    Connection = DMConexao.FDConexao
    SQL.Strings = (
      'select * from cad_editora')
    Left = 80
    Top = 48
    object FDQEditoraid_editora: TIntegerField
      FieldName = 'id_editora'
      Origin = 'id_editora'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQEditoranome: TWideStringField
      FieldName = 'nome'
      Origin = 'nome'
      Size = 60
    end
    object FDQEditoracnpj: TWideStringField
      FieldName = 'cnpj'
      Origin = 'cnpj'
      Size = 45
    end
  end
  object DSEditora: TDataSource
    DataSet = FDQEditora
    OnStateChange = DSEditoraStateChange
    Left = 136
    Top = 32
  end
  object FDQComboEditora: TFDQuery
    Connection = DMConexao.FDConexao
    SQL.Strings = (
      'select * from cad_editora')
    Left = 223
    Top = 48
  end
end
