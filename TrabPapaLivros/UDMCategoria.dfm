object DMCategoria: TDMCategoria
  OldCreateOrder = False
  Height = 150
  Width = 309
  object FDQCategoria: TFDQuery
    Connection = DMConexao.FDConexao
    SQL.Strings = (
      'select * from cad_categoria')
    Left = 80
    Top = 48
    object FDQCategoriaid_categoria: TIntegerField
      FieldName = 'id_categoria'
      Origin = 'id_categoria'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQCategorianome: TWideStringField
      FieldName = 'nome'
      Origin = 'nome'
      Size = 45
    end
  end
  object DSCategoria: TDataSource
    DataSet = FDQCategoria
    OnStateChange = DSCategoriaStateChange
    Left = 136
    Top = 32
  end
  object FDQComboCategoria: TFDQuery
    Connection = DMConexao.FDConexao
    SQL.Strings = (
      'select * from cad_categoria')
    Left = 223
    Top = 48
  end
end
