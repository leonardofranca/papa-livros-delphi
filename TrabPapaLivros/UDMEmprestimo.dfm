object DMEmprestimo: TDMEmprestimo
  OldCreateOrder = False
  Height = 243
  Width = 557
  object rEmprestimos: TfrxReport
    Version = '6.3.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Padr'#227'o'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 43626.899099213000000000
    ReportOptions.LastChange = 43626.899099213000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 416
    Top = 96
    Datasets = <
      item
        DataSet = frxDsAutor
        DataSetName = 'frxDsAutor'
      end
      item
        DataSet = frxDSLivros
        DataSetName = 'frxDSLivros'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 7.559060000000000000
          Width = 94.488250000000000000
          Height = 30.236240000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Livros')
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 7.559060000000000000
          Width = 34.015770000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 102.047310000000000000
        Width = 718.110700000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 147.401670000000000000
        Width = 718.110700000000000000
        DataSet = frxDSLivros
        DataSetName = 'frxDSLivros'
        RowCount = 0
        object frxDSLivrostitulo: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 200.315090000000000000
          Height = 18.897650000000000000
          DataField = 'titulo'
          DataSet = frxDSLivros
          DataSetName = 'frxDSLivros'
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDSLivros."titulo"]')
        end
        object frxDSLivrosquantidade: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'quantidade'
          DataSet = frxDSLivros
          DataSetName = 'frxDSLivros'
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDSLivros."quantidade"]')
        end
        object frxDSLivrosdescricao: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 313.700990000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'descricao'
          DataSet = frxDSLivros
          DataSetName = 'frxDSLivros'
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDSLivros."descricao"]')
        end
      end
    end
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    PDFStandard = psNone
    PDFVersion = pv17
    Left = 416
    Top = 24
  end
  object FDQAutor: TFDQuery
    Connection = DMConexao.FDConexao
    SQL.Strings = (
      'select * from cad_autor')
    Left = 488
    Top = 88
  end
  object FDQLivros: TFDQuery
    CachedUpdates = True
    Connection = DMConexao.FDConexao
    SchemaAdapter = FDSA
    SQL.Strings = (
      'select * from cad_livro')
    Left = 288
    Top = 32
    object FDQLivrosid_livro: TIntegerField
      FieldName = 'id_livro'
      Origin = 'id_livro'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQLivrostitulo: TWideStringField
      FieldName = 'titulo'
      Origin = 'titulo'
      Size = 45
    end
    object FDQLivrosdescricao: TWideStringField
      FieldName = 'descricao'
      Origin = 'descricao'
      Size = 255
    end
    object FDQLivrosstatus: TWideStringField
      FieldName = 'status'
      Origin = 'status'
      FixedChar = True
      Size = 1
    end
    object FDQLivrosquantidade: TIntegerField
      FieldName = 'quantidade'
      Origin = 'quantidade'
    end
    object FDQLivrosdata_publicacao: TDateField
      FieldName = 'data_publicacao'
      Origin = 'data_publicacao'
    end
    object FDQLivrosautor_id: TIntegerField
      FieldName = 'autor_id'
      Origin = 'autor_id'
    end
    object FDQLivroscategoria_id: TIntegerField
      FieldName = 'categoria_id'
      Origin = 'categoria_id'
    end
    object FDQLivroseditora_id: TIntegerField
      FieldName = 'editora_id'
      Origin = 'editora_id'
    end
  end
  object frxDsAutor: TfrxDBDataset
    UserName = 'frxDsAutor'
    CloseDataSource = False
    DataSet = FDQAutor
    BCDToCurrency = False
    Left = 496
    Top = 160
  end
  object frxDSLivros: TfrxDBDataset
    UserName = 'frxDSLivros'
    CloseDataSource = False
    DataSet = FDQLivros
    BCDToCurrency = False
    Left = 416
    Top = 160
  end
  object FDQEmprestimo: TFDQuery
    CachedUpdates = True
    Connection = DMConexao.FDConexao
    SchemaAdapter = FDSA
    UpdateOptions.AssignedValues = [uvEDelete, uvEInsert, uvEUpdate, uvGeneratorName]
    UpdateOptions.GeneratorName = 'cad_emprestimo_id_emprestimo_seq'
    UpdateOptions.KeyFields = 'id_emprestimo'
    UpdateOptions.AutoIncFields = 'id_emprestimo'
    SQL.Strings = (
      'select * from cad_emprestimo')
    Left = 40
    Top = 40
    object FDQEmprestimoid_emprestimo: TIntegerField
      FieldName = 'id_emprestimo'
      Origin = 'id_emprestimo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQEmprestimoid_usuario: TIntegerField
      FieldName = 'id_usuario'
      Origin = 'id_usuario'
    end
    object FDQEmprestimodt_emprestimo: TDateField
      FieldName = 'dt_emprestimo'
      Origin = 'dt_emprestimo'
    end
  end
  object FDQEmprestimoLivro: TFDQuery
    CachedUpdates = True
    IndexFieldNames = 'id_emprestimo'
    AggregatesActive = True
    DetailFields = 'id_emprestimo'
    Connection = DMConexao.FDConexao
    SchemaAdapter = FDSA
    FetchOptions.AssignedValues = [evDetailCascade]
    FetchOptions.DetailCascade = True
    UpdateOptions.AutoIncFields = 'id_emprestimo'
    SQL.Strings = (
      'select * from cad_emprestimo_livro where id_livro=:id_livro')
    Left = 128
    Top = 24
    ParamData = <
      item
        Name = 'ID_LIVRO'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object FDQEmprestimoLivroid_livro: TIntegerField
      FieldName = 'id_livro'
      Origin = 'id_livro'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQEmprestimoLivroid_emprestimo: TIntegerField
      FieldName = 'id_emprestimo'
      Origin = 'id_emprestimo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQEmprestimoLivroquantidade: TIntegerField
      FieldName = 'quantidade'
      Origin = 'quantidade'
    end
    object FDQEmprestimoLivrodt_devolucao: TDateField
      FieldName = 'dt_devolucao'
      Origin = 'dt_devolucao'
    end
  end
  object FDQUsuario: TFDQuery
    CachedUpdates = True
    Connection = DMConexao.FDConexao
    SchemaAdapter = FDSA
    SQL.Strings = (
      'select * from cad_usuarios')
    Left = 208
    Top = 40
    object FDQUsuarioid_usuario: TIntegerField
      FieldName = 'id_usuario'
      Origin = 'id_usuario'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQUsuarionm_usuario: TWideStringField
      FieldName = 'nm_usuario'
      Origin = 'nm_usuario'
      Size = 40
    end
    object FDQUsuariologin: TWideStringField
      FieldName = 'login'
      Origin = '"login"'
      Size = 30
    end
    object FDQUsuariosenha: TWideStringField
      FieldName = 'senha'
      Origin = 'senha'
      Size = 30
    end
    object FDQUsuarionivel: TWideStringField
      FieldName = 'nivel'
      Origin = 'nivel'
      Size = 50
    end
    object FDQUsuariosexo: TWideStringField
      FieldName = 'sexo'
      Origin = 'sexo'
      FixedChar = True
      Size = 1
    end
    object FDQUsuariodt_nascimento: TDateField
      FieldName = 'dt_nascimento'
      Origin = 'dt_nascimento'
    end
  end
  object FDSA: TFDSchemaAdapter
    Left = 72
    Top = 120
  end
  object FDQBuscaLivros: TFDQuery
    Connection = DMConexao.FDConexao
    SQL.Strings = (
      'select * from cad_livro where titulo like :pnome')
    Left = 152
    Top = 136
    ParamData = <
      item
        Name = 'PNOME'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    object FDQBuscaLivrosid_livro: TIntegerField
      FieldName = 'id_livro'
      Origin = 'id_livro'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQBuscaLivrostitulo: TWideStringField
      FieldName = 'titulo'
      Origin = 'titulo'
      Size = 45
    end
    object FDQBuscaLivrosdescricao: TWideStringField
      FieldName = 'descricao'
      Origin = 'descricao'
      Size = 255
    end
    object FDQBuscaLivrosstatus: TWideStringField
      FieldName = 'status'
      Origin = 'status'
      FixedChar = True
      Size = 1
    end
    object FDQBuscaLivrosquantidade: TIntegerField
      FieldName = 'quantidade'
      Origin = 'quantidade'
    end
    object FDQBuscaLivrosdata_publicacao: TDateField
      FieldName = 'data_publicacao'
      Origin = 'data_publicacao'
    end
    object FDQBuscaLivrosautor_id: TIntegerField
      FieldName = 'autor_id'
      Origin = 'autor_id'
    end
    object FDQBuscaLivroscategoria_id: TIntegerField
      FieldName = 'categoria_id'
      Origin = 'categoria_id'
    end
    object FDQBuscaLivroseditora_id: TIntegerField
      FieldName = 'editora_id'
      Origin = 'editora_id'
    end
  end
end
