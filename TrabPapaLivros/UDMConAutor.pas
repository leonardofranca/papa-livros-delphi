unit UDMConAutor;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TDMAutor = class(TDataModule)
    FDQAutor: TFDQuery;
    DSAutor: TDataSource;
    FDQComboAutor: TFDQuery;
    FDQAutorid_autor: TIntegerField;
    FDQAutornome: TWideStringField;
    procedure DSAutorStateChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMAutor: TDMAutor;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UCadAutor;

{$R *.dfm}

procedure TDMAutor.DSAutorStateChange(Sender: TObject);
begin
  with FAutores do
    begin
       BNovo.Enabled := DSAutor.State in [dsBrowse];
      BSalvar.Enabled := DSAutor.State in [dsInsert, dsEdit];
      BCancelar.Enabled := Bsalvar.Enabled;
      BEditar.Enabled := (BNovo.Enabled) and (not(FDQAutor.IsEmpty));
      BExcluir.Enabled := BEditar.Enabled;
    end;

end;

end.
