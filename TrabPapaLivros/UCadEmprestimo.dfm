object FCadEmprestimo: TFCadEmprestimo
  Left = 0
  Top = 0
  Caption = 'FCadEmprestimo'
  ClientHeight = 390
  ClientWidth = 413
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 32
    Top = 144
    Width = 91
    Height = 13
    Caption = 'Data de Devolucao'
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 413
    Height = 41
    Align = alTop
    Color = clBtnShadow
    ParentBackground = False
    TabOrder = 0
    ExplicitWidth = 427
    object BNovo: TBitBtn
      Left = 8
      Top = 9
      Width = 80
      Height = 25
      Caption = 'Novo'
      TabOrder = 0
      OnClick = BNovoClick
    end
    object BEditar: TBitBtn
      Left = 90
      Top = 9
      Width = 80
      Height = 25
      Caption = 'Editar'
      TabOrder = 1
      OnClick = BEditarClick
    end
    object BSalvar: TBitBtn
      Left = 172
      Top = 9
      Width = 80
      Height = 25
      Caption = 'Salvar'
      TabOrder = 2
      OnClick = BSalvarClick
    end
    object BCancelar: TBitBtn
      Left = 254
      Top = 9
      Width = 80
      Height = 25
      Caption = 'Cancelar'
      TabOrder = 3
      OnClick = BCancelarClick
    end
    object BExcluir: TBitBtn
      Left = 337
      Top = 9
      Width = 80
      Height = 25
      Caption = 'Excluir'
      TabOrder = 4
      OnClick = BExcluirClick
    end
  end
  object GroupBox1: TGroupBox
    Left = 1
    Top = 47
    Width = 409
    Height = 77
    Caption = 'Usuario'
    TabOrder = 1
    object Nome: TLabel
      Left = 77
      Top = 18
      Width = 27
      Height = 13
      Caption = 'Nome'
    end
    object Label1: TLabel
      Left = 16
      Top = 18
      Width = 23
      Height = 13
      Caption = 'C'#243'd.'
    end
    object DBEdit1: TDBEdit
      Left = 16
      Top = 37
      Width = 50
      Height = 21
      Color = cl3DLight
      DataField = 'id_usuario'
      DataSource = DSUsuarios
      Enabled = False
      TabOrder = 0
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 77
      Top = 37
      Width = 319
      Height = 21
      DataField = 'id_usuario'
      DataSource = DSEmprestimo
      KeyField = 'id_usuario'
      ListField = 'nm_usuario'
      ListSource = DSUsuarios
      TabOrder = 1
      OnExit = DBLookupComboBox1Exit
    end
  end
  object GroupBox2: TGroupBox
    Left = 3
    Top = 122
    Width = 410
    Height = 270
    Caption = 'Livros'
    TabOrder = 2
    object Label3: TLabel
      Left = 78
      Top = 69
      Width = 26
      Height = 13
      Caption = 'Titulo'
    end
    object Label4: TLabel
      Left = 17
      Top = 69
      Width = 23
      Height = 13
      Caption = 'C'#243'd.'
    end
    object Label5: TLabel
      Left = 291
      Top = 42
      Width = 34
      Height = 13
      Caption = 'Quant.'
    end
    object CBProdutos: TDBLookupComboBox
      Left = 78
      Top = 88
      Width = 200
      Height = 21
      DataField = 'id_livro'
      DataSource = DSEmprestimoLivro
      KeyField = 'id_livro'
      ListField = 'titulo'
      ListSource = DSLivros
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 17
      Top = 88
      Width = 50
      Height = 21
      Color = cl3DLight
      DataField = 'id_livro'
      DataSource = DSEmprestimoLivro
      Enabled = False
      TabOrder = 3
    end
    object DBEdit3: TDBEdit
      Left = 291
      Top = 61
      Width = 105
      Height = 21
      DataField = 'quantidade'
      DataSource = DSEmprestimoLivro
      TabOrder = 1
    end
    object DBGrid1: TDBGrid
      Left = 16
      Top = 120
      Width = 380
      Height = 122
      DataSource = DSEmprestimoLivro
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 4
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'id_livro'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'id_emprestimo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'quantidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dt_devolucao'
          Visible = True
        end>
    end
    object BitBtn2: TBitBtn
      Left = 347
      Top = 88
      Width = 50
      Height = 25
      Glyph.Data = {
        36020000424D3602000000000000360100002800000010000000100000000100
        08000000000000010000C40E0000C40E00004000000040000000F7F7F700F0F0
        F000E6E6E600DEDEDE00D3D3D300CCCCCC00C4C4C400BBBBBB008E8EE8008484
        E6007F7FE5007373E3006D6DE2006262E0005A5ADE005252DC004A4ADB004444
        DA004141D9003939D700666666003131D6002828D4002828D10059595A002323
        D2002222CB001F1FC800505051001C1CC9001919CD00474748000E0EC3000F0F
        BD000C0CC500414142000A0ABA003A3A42000308B5000606B30039393A000606
        AF000404B50000129A0000109400333333000000AE00000E8C000000A5000000
        990000008D00282829000000840000007A000000720020202100000066000000
        5C00181819000000540008080900000000000201FE00000000003C3C3D3D3D3D
        3D3D3D3D3D3D3D3D3D3D3A212930313234343535353638393B3D371B21273031
        3132323434363638393D33171D202A2E3031313234353636383D2D13171E202A
        2E30313132343535363D2D1215191E22262B2B2F32323434353D2811131A242E
        2E30313131323434343D2810121701030404050607063132343D281011110001
        0102020203032932343D230F0F0F0F0F11131516191E2731343D1F0E0E0F0F10
        1215191E222A2E31323D1C0D0D0D0E0F111315191E202A30313D180B0B0C0D0F
        10121315161E2027303D14090A0B0D0E0F11121313161B21293D1408090B0C0E
        0F0F10101213151A213C181414181C1F23232828282D33373A3C}
      TabOrder = 2
      OnClick = BitBtn2Click
    end
  end
  object BitBtn1: TBitBtn
    Left = 299
    Top = 208
    Width = 50
    Height = 25
    Glyph.Data = {
      36030000424D3603000000000000360200002800000010000000100000000100
      08000000000000010000C40E0000C40E00008000000080000000FFFFFF00F5F5
      F500EBEBEB00E6E6E600E2E2E200D6D6D600BFBFBF00B8B8B800FFBB8E00AFAF
      AF00FFB58400FFB27F00FFAC7400FFA56900FFA26400FF9C5A00FF995500FF96
      5000FF924A00FF8D4100FF893A00FE863600FF833100FA7C2800FE7C2600FD7B
      2400F6741E0066666600F7721A00F4690C00EE680F00E6671200E9660F00EF67
      0C005A595900EC610500E85F0400DE5C060051505000E65D0200E15A0000D759
      0500D4550000CE52000048474700C850000046454500BC4B000042414100C54A
      0000B5480000B1470000AB440000423A3A003A393900A54200009D3F0000993D
      000033333300943B00008D38000086360000813400007C32000029282800732E
      0000692A0000212020001918180009080800000000000201FE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000454546464646
      46464646464646464646441F292D2F33373838383B3C3E414246431A20252B31
      32323437383B3D3F414640171A1E252A2B2B2F3737393C3D3E463A15171C2127
      280506373438383B3C463A1316191C1D2705073B323437383B46361314172127
      230509383B37373738463612131701030405090907062F343846361212130001
      0102050505052B333746301111100F0D0F01042324282B2F34462E0F0F101112
      0D0103241D242A313346260D0E0E0F110F00011E1C1E252B2F46220C0C0C0E10
      10131617171C1E252B461B0A0B0C0E0F1112131414171A1E29461B080A0C0D0F
      101112121314161A1F45221B1B22262C30303636363A40434445}
    TabOrder = 3
    OnClick = BitBtn1Click
  end
  object DBEdit4: TDBEdit
    Left = 17
    Top = 164
    Width = 262
    Height = 21
    DataField = 'dt_devolucao'
    DataSource = DSEmprestimoLivro
    TabOrder = 4
  end
  object DSEmprestimo: TDataSource
    DataSet = DMEmprestimo.FDQEmprestimo
    Left = 232
    Top = 304
  end
  object DSUsuarios: TDataSource
    DataSet = DMEmprestimo.FDQUsuario
    Left = 312
    Top = 296
  end
  object DSLivros: TDataSource
    DataSet = DMEmprestimo.FDQLivros
    Left = 152
    Top = 312
  end
  object DSEmprestimoLivro: TDataSource
    DataSet = DMEmprestimo.FDQEmprestimoLivro
    Left = 56
    Top = 304
  end
end
