unit UCadAutor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Mask, Vcl.DBCtrls,
  Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.Buttons, Vcl.ComCtrls;

type
  TFAutores = class(TForm)
    PCAutores: TPageControl;
    TCadastro: TTabSheet;
    BNovo: TBitBtn;
    BEditar: TBitBtn;
    BSalvar: TBitBtn;
    BCancelar: TBitBtn;
    BExcluir: TBitBtn;
    TConsulta: TTabSheet;
    EPesquisar: TEdit;
    BPesquisar: TBitBtn;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure BPesquisarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BSalvarClick(Sender: TObject);
    procedure BEditarClick(Sender: TObject);
    procedure BNovoClick(Sender: TObject);
    procedure BCancelarClick(Sender: TObject);
    procedure BExcluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAutores: TFAutores;

implementation

{$R *.dfm}

uses UDMConAutor;

procedure TFAutores.BCancelarClick(Sender: TObject);
begin
  DMAutor.FDQAutor.Cancel;
end;

procedure TFAutores.BEditarClick(Sender: TObject);
begin
    DMAutor.FDQAutor.Edit;
end;

procedure TFAutores.BExcluirClick(Sender: TObject);
begin
  DMAutor.FDQAutor.Delete;
end;

procedure TFAutores.BNovoClick(Sender: TObject);
begin
  DMAutor.FDQAutor.Insert;
end;

procedure TFAutores.BPesquisarClick(Sender: TObject);
begin
  with DMAutor do
  begin
    FDQAutor.Close;
    FDQAutor.SQL.Clear;
    FDQAutor.SQL.Add('select * from cad_autor where nome like :Pautor ');
    FDQAutor.ParamByName('Pautor').Value := '%' + EPesquisar.Text + '%';
    FDQAutor.Open;
  end;
end;

procedure TFAutores.BSalvarClick(Sender: TObject);
begin
  DMAutor.FDQAutor.Post;
end;

procedure TFAutores.DBGrid1DblClick(Sender: TObject);
begin
  PCAutores.ActivePage := TCadastro;
end;

procedure TFAutores.FormActivate(Sender: TObject);
begin
  with DMAutor do
  begin
    FDQAutor.Close;
    FDQAutor.SQL.Clear;
    FDQAutor.SQL.Add('select * from cad_autor');
    FDQAutor.Open;
  end;

  PCAutores.ActivePage := TCadastro;
end;

procedure TFAutores.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FAutores := nil;
  FAutores.Free;
  DMAutor := nil;
  DMAutor.Free;
end;

end.
