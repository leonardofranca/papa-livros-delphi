object FAutores: TFAutores
  Left = 0
  Top = 0
  Caption = 'Cadastro de Autores'
  ClientHeight = 201
  ClientWidth = 371
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PCAutores: TPageControl
    Left = 0
    Top = 0
    Width = 369
    Height = 201
    ActivePage = TCadastro
    TabOrder = 0
    object TCadastro: TTabSheet
      Caption = 'Cadastro'
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 40
        Height = 13
        Caption = 'id_autor'
        FocusControl = DBEdit1
      end
      object Label2: TLabel
        Left = 16
        Top = 56
        Width = 26
        Height = 13
        Caption = 'nome'
        FocusControl = DBEdit2
      end
      object BNovo: TBitBtn
        Left = 19
        Top = 112
        Width = 60
        Height = 25
        Caption = 'Novo'
        TabOrder = 0
        OnClick = BNovoClick
      end
      object BEditar: TBitBtn
        Left = 85
        Top = 112
        Width = 60
        Height = 25
        Caption = 'Editar'
        TabOrder = 1
        OnClick = BEditarClick
      end
      object BSalvar: TBitBtn
        Left = 155
        Top = 112
        Width = 60
        Height = 25
        Caption = 'Salvar'
        TabOrder = 2
        OnClick = BSalvarClick
      end
      object BCancelar: TBitBtn
        Left = 223
        Top = 112
        Width = 60
        Height = 25
        Caption = 'Cancelar'
        TabOrder = 3
        OnClick = BCancelarClick
      end
      object BExcluir: TBitBtn
        Left = 289
        Top = 112
        Width = 60
        Height = 25
        Caption = 'Excluir'
        TabOrder = 4
        OnClick = BExcluirClick
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 32
        Width = 134
        Height = 21
        DataField = 'id_autor'
        DataSource = DMAutor.DSAutor
        Enabled = False
        TabOrder = 5
      end
      object DBEdit2: TDBEdit
        Left = 16
        Top = 72
        Width = 333
        Height = 21
        DataField = 'nome'
        DataSource = DMAutor.DSAutor
        TabOrder = 6
      end
    end
    object TConsulta: TTabSheet
      Caption = 'Consulta'
      ImageIndex = 1
      object EPesquisar: TEdit
        Left = 16
        Top = 16
        Width = 225
        Height = 21
        TabOrder = 0
      end
      object BPesquisar: TBitBtn
        Left = 247
        Top = 14
        Width = 98
        Height = 25
        Caption = 'Pesquisar'
        TabOrder = 1
        OnClick = BPesquisarClick
      end
      object DBGrid1: TDBGrid
        Left = 16
        Top = 45
        Width = 329
        Height = 120
        DataSource = DMAutor.DSAutor
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = DBGrid1DblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'id_autor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nome'
            Visible = True
          end>
      end
    end
  end
end
