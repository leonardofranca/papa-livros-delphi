unit UDMLivros;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TDMLivros = class(TDataModule)
    FDQLivros: TFDQuery;
    FDQComboLivros: TFDQuery;
    DSLivros: TDataSource;
    FDQLivrosid_livro: TIntegerField;
    FDQLivrostitulo: TWideStringField;
    FDQLivrosdescricao: TWideStringField;
    FDQLivrosstatus: TWideStringField;
    FDQLivrosquantidade: TIntegerField;
    FDQLivrosdata_publicacao: TDateField;
    FDQLivrosautor_id: TIntegerField;
    FDQLivroscategoria_id: TIntegerField;
    FDQLivroseditora_id: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMLivros: TDMLivros;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UDMConexao;

{$R *.dfm}

end.
