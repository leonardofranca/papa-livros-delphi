object DMLivros: TDMLivros
  OldCreateOrder = False
  Height = 139
  Width = 278
  object FDQLivros: TFDQuery
    Connection = DMConexao.FDConexao
    SQL.Strings = (
      'select * from cad_livro')
    Left = 104
    Top = 48
    object FDQLivrosid_livro: TIntegerField
      FieldName = 'id_livro'
      Origin = 'id_livro'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQLivrostitulo: TWideStringField
      FieldName = 'titulo'
      Origin = 'titulo'
      Size = 45
    end
    object FDQLivrosdescricao: TWideStringField
      FieldName = 'descricao'
      Origin = 'descricao'
      Size = 255
    end
    object FDQLivrosstatus: TWideStringField
      FieldName = 'status'
      Origin = 'status'
      FixedChar = True
      Size = 1
    end
    object FDQLivrosquantidade: TIntegerField
      FieldName = 'quantidade'
      Origin = 'quantidade'
    end
    object FDQLivrosdata_publicacao: TDateField
      FieldName = 'data_publicacao'
      Origin = 'data_publicacao'
    end
    object FDQLivrosautor_id: TIntegerField
      FieldName = 'autor_id'
      Origin = 'autor_id'
    end
    object FDQLivroscategoria_id: TIntegerField
      FieldName = 'categoria_id'
      Origin = 'categoria_id'
    end
    object FDQLivroseditora_id: TIntegerField
      FieldName = 'editora_id'
      Origin = 'editora_id'
    end
  end
  object FDQComboLivros: TFDQuery
    Left = 184
    Top = 40
  end
  object DSLivros: TDataSource
    DataSet = FDQLivros
    Left = 32
    Top = 48
  end
end
