unit UCadCategoria;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Mask, Vcl.DBCtrls,
  Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.Buttons, Vcl.ComCtrls;

type
  TFCadCategoria = class(TForm)
    PCCategoria: TPageControl;
    TCadastro: TTabSheet;
    BNovo: TBitBtn;
    BEditar: TBitBtn;
    BSalvar: TBitBtn;
    BCancelar: TBitBtn;
    BExcluir: TBitBtn;
    TConsulta: TTabSheet;
    EPesquisar: TEdit;
    BPesquisar: TBitBtn;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure BCancelarClick(Sender: TObject);
    procedure BNovoClick(Sender: TObject);
    procedure BEditarClick(Sender: TObject);
    procedure BSalvarClick(Sender: TObject);
    procedure BExcluirClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BPesquisarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadCategoria: TFCadCategoria;

implementation

{$R *.dfm}

uses UDMCategoria;

procedure TFCadCategoria.BCancelarClick(Sender: TObject);
begin
  DMCategoria.FDQCategoria.Cancel;
end;

procedure TFCadCategoria.BEditarClick(Sender: TObject);
begin
  DMCategoria.FDQCategoria.Edit;
end;

procedure TFCadCategoria.BExcluirClick(Sender: TObject);
begin
  DMCategoria.FDQCategoria.Delete;
end;

procedure TFCadCategoria.BNovoClick(Sender: TObject);
begin
  DMCategoria.FDQCategoria.Insert;
end;

procedure TFCadCategoria.BPesquisarClick(Sender: TObject);
begin
  with DMCategoria do
  begin
    FDQCategoria.Close;
    FDQCategoria.SQL.Clear;
    FDQCategoria.SQL.Add('select * from cad_categoria where nome like :Pcategoria ');
    FDQCategoria.ParamByName('Pcategoria').Value := '%' + EPesquisar.Text + '%';
    FDQCategoria.Open;
  end;
end;

procedure TFCadCategoria.BSalvarClick(Sender: TObject);
begin
  DMCategoria.FDQCategoria.Post;
end;

procedure TFCadCategoria.DBGrid1DblClick(Sender: TObject);
begin
  PCCategoria.ActivePage := TCadastro;
end;

procedure TFCadCategoria.FormActivate(Sender: TObject);
begin
with DMCategoria do
  begin
    FDQCategoria.Close;
    FDQCategoria.SQL.Clear;
    FDQCategoria.SQL.Add('select * from cad_categoria');
    FDQCategoria.Open;
  end;

  PCCategoria.ActivePage := TCadastro;
end;

procedure TFCadCategoria.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FCadCategoria := nil;
  FCadCategoria.Free;
  DMCategoria := nil;
  DMCategoria.Free;
end;

end.
