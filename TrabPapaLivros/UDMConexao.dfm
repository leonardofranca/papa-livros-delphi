object DMConexao: TDMConexao
  OldCreateOrder = False
  Height = 310
  Width = 508
  object FDConexao: TFDConnection
    Params.Strings = (
      'Database=papa_livros'
      'User_Name=postgres'
      'Password=postgre'
      'Server=localhost'
      'DriverID=PG')
    Left = 192
    Top = 136
  end
  object FDPgDriver: TFDPhysPgDriverLink
    VendorHome = 'C:\Projetos Delphi\TrabPapaLivros\Win32\Debug'
    Left = 304
    Top = 240
  end
  object FDQUsuario: TFDQuery
    Connection = FDConexao
    SQL.Strings = (
      'select * from cad_usuarios where login=:plogin')
    Left = 280
    Top = 168
    ParamData = <
      item
        Name = 'PLOGIN'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    object FDQUsuarioid_usuario: TIntegerField
      FieldName = 'id_usuario'
      Origin = 'id_usuario'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQUsuarionm_usuario: TWideStringField
      FieldName = 'nm_usuario'
      Origin = 'nm_usuario'
      Size = 40
    end
    object FDQUsuariologin: TWideStringField
      FieldName = 'login'
      Origin = '"login"'
      Size = 30
    end
    object FDQUsuariosenha: TWideStringField
      FieldName = 'senha'
      Origin = 'senha'
      Size = 30
    end
    object FDQUsuarionivel: TWideStringField
      FieldName = 'nivel'
      Origin = 'nivel'
      Size = 50
    end
  end
  object FDQNivel: TFDQuery
    Connection = FDConexao
    SQL.Strings = (
      'select * from cad_usuarios where id_usuario =:pid')
    Left = 72
    Top = 32
    ParamData = <
      item
        Name = 'PID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object FDQNivelid_usuario: TIntegerField
      FieldName = 'id_usuario'
      Origin = 'id_usuario'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQNivelnm_usuario: TWideStringField
      FieldName = 'nm_usuario'
      Origin = 'nm_usuario'
      Size = 40
    end
    object FDQNivellogin: TWideStringField
      FieldName = 'login'
      Origin = '"login"'
      Size = 30
    end
    object FDQNivelsenha: TWideStringField
      FieldName = 'senha'
      Origin = 'senha'
      Size = 30
    end
    object FDQNivelnivel: TWideStringField
      FieldName = 'nivel'
      Origin = 'nivel'
      Size = 50
    end
  end
end
