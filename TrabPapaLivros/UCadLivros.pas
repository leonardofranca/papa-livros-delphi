unit UCadLivros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.DBCtrls, Vcl.Mask, Vcl.Buttons, Vcl.ComCtrls;

type
  TFCadLivros = class(TForm)
    PCLivros: TPageControl;
    TCadastro: TTabSheet;
    BNovo: TBitBtn;
    BEditar: TBitBtn;
    BSalvar: TBitBtn;
    BCancelar: TBitBtn;
    BExcluir: TBitBtn;
    TConsulta: TTabSheet;
    EPesquisar: TEdit;
    BBuscar: TBitBtn;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DSLivros: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    Label7: TLabel;
    Label8: TLabel;
    Autor: TLabel;
    Label9: TLabel;
    DBLookupComboBox2: TDBLookupComboBox;
    Label10: TLabel;
    DBLookupComboBox3: TDBLookupComboBox;
    Label11: TLabel;
    DSAutor: TDataSource;
    DSEditora: TDataSource;
    DSCategoria: TDataSource;
    procedure BNovoClick(Sender: TObject);
    procedure BEditarClick(Sender: TObject);
    procedure BSalvarClick(Sender: TObject);
    procedure BCancelarClick(Sender: TObject);
    procedure BExcluirClick(Sender: TObject);
    procedure BBuscarClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DSLivrosStateChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadLivros: TFCadLivros;

implementation

{$R *.dfm}

uses UDMCategoria, UDMConAutor, UDMEditora, UDMLivros, UDMConexao;

procedure TFCadLivros.BBuscarClick(Sender: TObject);
begin
  with DMLivros do
  begin
    FDQLivros.Close;
    FDQLivros.SQL.Clear;
    FDQLivros.SQL.Add('select * from cad_livro where titulo like :Ptitulo ');
    FDQLivros.ParamByName('Ptitulo').Value := '%'+EPesquisar.Text+'%';
    FDQLivros.Open;
  end;
end;

procedure TFCadLivros.BCancelarClick(Sender: TObject);
begin
  DMLivros.FDQLivros.Cancel;
end;

procedure TFCadLivros.BEditarClick(Sender: TObject);
begin
  DMLivros.FDQLivros.Edit;
end;

procedure TFCadLivros.BExcluirClick(Sender: TObject);
begin
  DMLivros.FDQLivros.Delete;
end;

procedure TFCadLivros.BNovoClick(Sender: TObject);
begin
  DMLivros.FDQLivros.Insert;
end;

procedure TFCadLivros.BSalvarClick(Sender: TObject);
begin
  DMLivros.FDQLivros.Post;
end;

procedure TFCadLivros.DBGrid1DblClick(Sender: TObject);
begin
    PCLivros.ActivePage := TCadastro;
end;

procedure TFCadLivros.DSLivrosStateChange(Sender: TObject);
begin
  BNovo.Enabled := DSLivros.State in [dsBrowse];
    BSalvar.Enabled := DSLivros.State in [dsInsert, dsEdit];
    BCancelar.Enabled := Bsalvar.Enabled;
    BEditar.Enabled := (BNovo.Enabled) and (not(DMLivros.FDQLivros.IsEmpty));
    BExcluir.Enabled := BEditar.Enabled;
end;

procedure TFCadLivros.FormActivate(Sender: TObject);
begin
  with DMLivros do
  begin
    FDQLivros.Close;
    FDQLivros.SQL.Clear;
    FDQLivros.SQL.Add('select * from cad_livro');
    FDQLivros.Open;
  end;

  PCLivros.ActivePage:= TCadastro;
  DMEditora.FDQComboEditora.Open();
  DMAutor.FDQComboAutor.Open();
  DMCategoria.FDQComboCategoria.Open();
end;

procedure TFCadLivros.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FCadLivros := nil;
  FCadLivros.Free;
  DMLivros := nil;
  DMLivros.Free;
end;

end.
