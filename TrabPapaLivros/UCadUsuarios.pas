unit UCadUsuarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, System.ImageList, Vcl.ImgList,
  Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.Buttons, Vcl.Mask,
  Vcl.DBCtrls;

type
  TFCadUsuarios = class(TForm)
    PC: TPageControl;
    TCadastro: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ENm_usuario: TDBEdit;
    ELogin: TDBEdit;
    ESenha: TDBEdit;
    Edit1: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    TConsulta: TTabSheet;
    DBGrid1: TDBGrid;
    EBusca: TEdit;
    BitBtn6: TBitBtn;
    TV: TTreeView;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    DSUsuarios: TDataSource;
    ImageList1: TImageList;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    function remove_caractere(texto:string):string;
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure TVKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadUsuarios: TFCadUsuarios;

implementation

{$R *.dfm}

uses UPrincipal, UDMConexao, UDMUsuarios, ULogin;

function tfcadusuarios.remove_caractere(texto:string):string;
var
  p:integer;
begin
  p := Pos('&', texto);
  if p = 0 then
    texto := texto
  else
    texto := Copy(texto, 1, p - 1)+Copy(texto, p + 1, texto.Length);
  Result := texto;
end;

procedure TFCadUsuarios.TVKeyPress(Sender: TObject; var Key: Char);
var
  tn: TTreeNode;

begin
  if Key = #32 then
    begin
      tn := tv.Selected;
      if tn.StateIndex = 2 then
        tn.StateIndex := 1
      else
        tn.StateIndex := 2;
    end;
end;

procedure TFCadUsuarios.BitBtn1Click(Sender: TObject);
begin
  DMUsuarios.FDQUsuarios.Insert;
end;

procedure TFCadUsuarios.BitBtn2Click(Sender: TObject);
begin
  dmusuarios.FDQUsuarios.Edit;
end;

procedure TFCadUsuarios.BitBtn3Click(Sender: TObject);
begin
  dmusuarios.FDQUsuarios.Post;
  dmusuarios.FDQUsuarios.Close;
  dmusuarios.FDQUsuarios.Open;
end;

procedure TFCadUsuarios.BitBtn4Click(Sender: TObject);
begin
  dmusuarios.FDQUsuarios.Delete;
end;

procedure TFCadUsuarios.BitBtn5Click(Sender: TObject);
begin
  dmusuarios.FDQUsuarios.Cancel;
end;

procedure TFCadUsuarios.BitBtn7Click(Sender: TObject);
var
  npos, i, ix, iz: integer;
  texto, snivel : string;
  tn1, tn2, tn3: TTreeNode;
begin
  with tv.items do
  begin
    clear;
    npos := 1;
    if not DMUsuarios.FDQUsuarios.IsEmpty then
    begin
      snivel := DMUsuarios.FDQUsuariosnivel.Value;
      for i := 0 to fprincipal.Menu.Items.Count -1 do
      begin
        texto := fprincipal.Menu.Items[i].Caption;
        texto := remove_caractere(texto);
        if texto <> '-' then
        begin
          tn1 := Add(nil, texto);
          tn1.ImageIndex := 0;
          tn1.SelectedIndex := 0;

          if copy(snivel,npos,1) = 'S' then
            tn1.StateIndex := 2
          else
            tn1.StateIndex := 1;
          npos := npos+1;
          //nivel2
          for ix := 0 to fprincipal.Menu.Items[i].Count -1 do
          begin
            texto := fprincipal.Menu.Items[i].Items[ix].Caption;
            texto := remove_caractere(texto);
            if texto <> '-' then
            begin
              tn2 := AddChild(tn1, texto);
              tn2.ImageIndex := 0;
              tn2.SelectedIndex := 0;

              if copy(snivel,npos,1) = 'S' then
                tn2.StateIndex := 2
              else
                tn2.StateIndex := 1;
              npos := npos+1;

              //nivel3
              for iz := 0 to fprincipal.Menu.Items[i].Items[ix].Count -1 do
              begin
                texto := fprincipal.Menu.Items[i].Items[ix].Items[iz].Caption;
                texto := remove_caractere(texto);
                if texto <> '-' then
                begin
                  tn3 := AddChild(tn2, texto);
                  tn3.ImageIndex := 0;
                  tn3.SelectedIndex := 0;

                  if copy(snivel,npos,1) = 'S' then
                    tn3.StateIndex := 2
                  else
                    tn3.StateIndex := 1;
                  npos := npos+1;
                end;
              end;
            end;
          end;
        end;
      end;
    end;

  end;
end;

procedure TFCadUsuarios.BitBtn8Click(Sender: TObject);
var
  i: integer;
  tn: TTreeNode;
  snivel: string;
begin
  snivel := '';
  for i := 0 to tv.Items.Count - 1 do
  begin
    tn := tv.Items[i];
    if (tn.StateIndex = 1) then
      snivel := snivel + 'N'
    else
      snivel := snivel + 'S';
  end;

  if not DMUsuarios.FDQUsuarios.IsEmpty then
  begin
    DMUsuarios.FDQUsuarios.Edit;
    DMUsuarios.FDQUsuariosnivel.Value := snivel;
    DMUsuarios.FDQUsuarios.Post;
  end;

end;

procedure TFCadUsuarios.FormActivate(Sender: TObject);
begin
  DMUsuarios.FDQUsuarios.Open;
end;

end.
