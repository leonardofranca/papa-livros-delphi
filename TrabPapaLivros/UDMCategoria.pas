unit UDMCategoria;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TDMCategoria = class(TDataModule)
    FDQCategoria: TFDQuery;
    DSCategoria: TDataSource;
    FDQComboCategoria: TFDQuery;
    FDQCategoriaid_categoria: TIntegerField;
    FDQCategorianome: TWideStringField;
    procedure DSCategoriaStateChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMCategoria: TDMCategoria;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UCadCategoria;

{$R *.dfm}

procedure TDMCategoria.DSCategoriaStateChange(Sender: TObject);
begin
  with FCadCategoria do
    begin
       BNovo.Enabled := DSCategoria.State in [dsBrowse];
      BSalvar.Enabled := DSCategoria.State in [dsInsert, dsEdit];
      BCancelar.Enabled := Bsalvar.Enabled;
      BEditar.Enabled := (BNovo.Enabled) and (not(FDQCategoria.IsEmpty));
      BExcluir.Enabled := BEditar.Enabled;
    end;
end;

end.
