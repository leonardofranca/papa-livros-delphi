object DMCor: TDMCor
  OldCreateOrder = False
  Height = 192
  Width = 336
  object FDQAutor: TFDQuery
    Connection = DMConexao.FDConexao
    SQL.Strings = (
      'select * from cad_autor')
    Left = 48
    Top = 32
    object FDQAutorid_cor: TIntegerField
      FieldName = 'id_cor'
      Origin = 'id_cor'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQAutornm_cor: TWideStringField
      FieldName = 'nm_cor'
      Origin = 'nm_cor'
      Size = 50
    end
  end
  object DSAutor: TDataSource
    DataSet = FDQAutor
    OnDataChange = DSAutorDataChange
    Left = 128
    Top = 32
  end
  object FDQComboAutor: TFDQuery
    Connection = DMConexao.FDConexao
    SQL.Strings = (
      'select * from cad_cores')
    Left = 224
    Top = 32
  end
end
